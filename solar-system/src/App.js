import './App.css';
import React, { useState } from 'react';

function App() {

  const [isDarkTheme, setIsDarkTheme] = useState(false);
  const listColor = isDarkTheme ? '#66bb6a' : 'black';

  const changeTheme = () => {
    setIsDarkTheme(!isDarkTheme);
    document.body.classList.toggle('dark');
  };

  const planets = [
    'Mercury',
    'Venus',
    'Earth',
    'Mars',
    'Jupiter',
    'Saturn',
    'Uranus',
    'Neptune'
  ];

  const title = React.createElement(
    'h1',
    { style: { color: '#999', fontSize: '19px' } },
    'Solar system planets'
  );

  const planetList = (
    <ul className="planets-list" style={{ color: listColor }}>
      {planets.map((planet, index) => (
        <li key={index}>{planet}</li>
      ))}
    </ul>
  );

  const slider = (
    <label className="switch" htmlFor="checkbox">
      <input type="checkbox" id="checkbox" />
      <div className="slider round" onClick={changeTheme}></div>
    </label>
  )

  return (
    <div>
      {title}
      {planetList}
      <div>
        {slider}
      </div>
    </div>
  );
}

export default App;
